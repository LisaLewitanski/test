# Documentations

## Culture et logiciels libres

<a href="https://associationici.fr" title="Lien vers associationici.fr">
   <img alt="texte alternatif pour le lien image" src="http://www.pouvoirdagir.fr/wp-content/uploads/2017/01/fk3RqhTi_400x400.jpg" width="75" height="75" />
   </a>

<!--
  La liste est classés par ordre alphabetique de Framaservice

  /!\ Ne pas faire pointer vers la racine du dossier
      mais bien sur "index.html"
-->
<div>
  <div class="col-md-3 col-sm-6">
    <a href="framakey/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-usb"></p>
      <p><b class="violet">Frama</b><b class="bleu">key</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framalibre/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-linux"></p>
      <p><b class="violet">Frama</b><b class="bleu">libre</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framapack/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-download"></p>
      <p><b class="violet">Frama</b><b class="bleu">pack</b></p>
    </a>
  </div>
</div>
